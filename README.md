# README #

I created this project to assist Developers workflow.

What this does is firstly installs all required nodes for the project.

Builds the structure of the site folders to pass to backend developers.

runs grunt to minify all css and Js.

ensure that you have node.js installed.

make sure you are in the newly cloned repo's directory 

- cd [ location of your cloned repo ]

Fistly you need to run npm-install on the command line ( run as admin *sudo)

- sudo npm install

build project 

- grunt

This will now make the directories and the developer can start the project.

To automatically update the css and Js just run grunt watch.

- grunt watch
